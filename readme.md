# Project Euler Nu

Script to fetch [project-euler](https://projecteuler.net/)
problems and their solutions.

Requires [Nu](https://www.nushell.sh/) and
[html2text](https://github.com/Alir3z4/html2text/).

## Installation

Use [Packer.nu](https://codeberg.org/packer.nu/packer.nu)

```nu
# .config/nushell/packages.nuon
{
	packages: [
		{source: 'https://gitlab.com/Abyst/project-euler-nu'}
	]
}
```

```sh
$ packer install
```

