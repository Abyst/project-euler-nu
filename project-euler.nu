
# Regular Expression Match
alias rem = rg -m1 -o -r$1

# Get the solution of a problem
export def solution [index: int] {
	let url = "https://raw.githubusercontent.com/luckytoilet/projecteuler-solutions/master/Solutions.md"
	http get $url | rem $"^($index)\\.\(.*\)$" | str trim
}

# Get the title and description of a problem
export def problem [index: int] {
	let url = $"https://projecteuler.info/problem=($index)"
	let problem = (http get $url | html2text)
	
	let re_description = '\[Server Time\](.*)Answer:'
	let re_title = '^## (.*)$'
	
	{
		title: ($problem | rem $re_title | str trim)
		description: (
			$problem |
			rem -U --multiline-dotall $re_description |
			str trim
		)
	}
}

# Get a markdown of the problem
export def markdown [index?: int] {
	let problem = if ($index != null) {
		problem $index
	} else {
		$in
	}
	[
		$'# ($problem.title)'
		$problem.description
	] | str join "\n\n"
}

# Create a directory with a Readme and solution of a problem
export def new [index: int] {
	let name = ($index | fill -a r -c 0 -w 3)
	let problem = (main $index)
	mkdir $name
	$problem | markdown | save -f $'($name)/readme.md'
	$problem.solution | save -f $'($name)/solution.txt'
}

# Get a problem with its solution
export def main [index: int] {
	problem $index | insert solution (solution $index)
}

